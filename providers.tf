# Enables access to secrets stored in vault.
# Vault authN is by workload identity using env variables
# stored in this workspace.
# See https://developer.hashicorp.com/terraform/cloud-docs/workspaces/dynamic-provider-credentials/vault-configuration.
provider "vault" {}

# Vault provides dynamic credentials to the AWS 
# provider. See bootstrapper and 
# https://developer.hashicorp.com/terraform/cloud-docs/workspaces/dynamic-provider-credentials/vault-backed/aws-configuration.
provider "aws" {
  alias = "vault_authenticated"
}


# HashiCorp internal provider that issues temporary credentials
# which the AWS provider uses
provider "doormat" {}

data "doormat_aws_credentials" "creds" {
  role_arn = var.doormat_arn
}

provider "aws" {
  region     = "eu-west-2" # London
  access_key = data.doormat_aws_credentials.creds.access_key
  secret_key = data.doormat_aws_credentials.creds.secret_key
  token      = data.doormat_aws_credentials.creds.token
}
