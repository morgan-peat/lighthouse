resource "random_pet" "default" {
  length    = 4
  separator = "-"
  prefix    = data.aws_regions.current.id
}


# This data block uses the vault-authenticated AWS provider which
# demonstrates best practice use in authenticating with dynamic
# credentials issued by vault.
data "aws_regions" "current" {
  provider = aws.vault_authenticated
}


resource "aws_s3_bucket" "example" {
  bucket = random_pet.default.id

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}