output "pet" {
  description = "A random pet name"
  value       = random_pet.default.id
}
