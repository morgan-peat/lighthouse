terraform {

  required_version = "~> 1.4.0" # Matches the TFE workspace - must be changed in sync

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.67.0"
    }
    doormat = {
      source  = "doormat.hashicorp.services/hashicorp-security/doormat"
      version = "~> 0.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.4.0"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "~> 3.15.0"
    }
  }
}
