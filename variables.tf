variable "doormat_arn" {
  description = "ARN of the role that the doormat provider uses to authN to AWS."
  type        = string
}
